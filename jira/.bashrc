# .bashrc

# Source global definitions
if [ -f /etc/bashrc ]; then
	. /etc/bashrc
fi

# User specific aliases and functions
alias psql="/apps/pgsql/pgsql/bin/psql -p 5448"
alias logtail="$HOME/bin/logtail.sh"
alias oemprep="$HOME/bin/oemprep.sh"
alias upgradeprep="$HOME/bin/upgradeprep.sh"
