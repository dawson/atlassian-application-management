# .bash_profile

# Get the aliases and functions
if [ -f ~/.bashrc ]; then
	. ~/.bashrc
fi

if [ -f ~/.atlassian_profile ]; then
    . ~/.atlassian_profile
fi

# User specific environment and startup programs

PATH=$PATH:$HOME/bin

export PATH
