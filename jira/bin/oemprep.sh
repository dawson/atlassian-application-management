#!/bin/bash
#Confluence only
BASE=/apps/atlassian/jira
APP_HOME=atlassian-jira
if [ $# -eq 1 ]; then
  BASE=$BASE/jira-$1
  CLASSES=$BASE/$APP_HOME/WEB-INF/classes

  echo "Prepping $BASE..."
  cp $BASE/conf/server.xml $BASE/conf/server.xml.OEM
  cp $BASE/bin/setenv.sh $BASE/bin/setenv.sh.OEM
# cp $BASE/$APP_HOME/WEB-INF/web.xml $BASE/$APP_HOME/WEB-INF/web.xml.OEM
  cp $CLASSES/jira-applications.properties $CLASSES/jira-application.properties.OEM
  cp $CLASSES/seraph-config.xml $CLASSES/seraph-config.xml.OEM
  cp $CLASSES/crowd.properties $CLASSES/crowd.properties.OEM

  #Image and icon stuff.  OEMs first, then we'll copy out of a dir because these don't change.
  cp $BASE/$APP_HOME/favicon.ico $BASE/$APP_HOME/favicon.ico.OEM
  cp $BASE/$APP_HOME/images/icons/favicon.png $BASE/$APP_HOME/images/icons/favicon.png.OEM
  cp $BASE/$APP_HOME/images/icons/favicon.ico $BASE/$APP_HOME/images/icons/favicon.ico.OEM
  
  #Add in the fixed helpful components
  cp ~/customizations/favicon.ico $BASE/$APP_HOME/favicon.ico
  cp ~/customizations/NIS-JIRA-Logo-2010.gif $BASE/$APP_HOME/NIS-JIRA-Logo-2010.gif

  echo "Done."
  exit 0

else
  echo "Usage: oemprep <version>"
  exit 1
fi

