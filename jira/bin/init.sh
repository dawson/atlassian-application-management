#!/bin/bash
JIRA_BIN=$JIRA_INSTALL/bin
CONF_BIN=$CONF_HOME/bin
CROWD_BIN=$CROWD_HOME/apache-tomcat/bin

validate_application(){
  application=$1
  case $application in 
    jira)
      APPDIR=$JIRA_BIN
      ;;
    confluence)
      APPDIR=$CONF_BIN
      ;;
    crowd)
      APPDIR=$CROWD_BIN
      ;;
  esac
  test -d $APPDIR
}

function start(){
  case $1 in
    jira) 
      $JIRA_BIN/startup.sh
    ;;
    confluence)
      $CONF_BIN/startup.sh
    ;;
    crowd)
      $CROWD_BIN/startup.sh
    ;;
  esac
}

function stop(){
  case $1 in
    jira) 
      $JIRA_BIN/shutdown.sh
    ;;
    confluence)
      $CONF_BIN/shutdown.sh
    ;;
    crowd)
      $CROWD_BIN/shutdown.sh
    ;;
  esac
}

function restart(){
  case $1 in
    jira) 
      $JIRA_BIN/shutdown.sh
      wait 30
      $JIRA_BIN/startup.sh
    ;;
    confluence)
      $CONF_BIN/shutdown.sh
      wait 30
      $CONF_BIN/startup.sh
    ;;
    crowd)
      $CROWD_BIN/shutdown.sh
      wait 30
      $CROWD_BIN/startup.sh
    ;;
  esac
}

#function start_all(){
#    start "crowd"
#    start "confluence"
#    start "jira"
#}
#
#function stop_all(){
#    stop "crowd"
#    stop "confluence"
#    stop "jira"
#
#}
#
#function restart_all(){
#    restart "crowd"
#    restart "confluence"
#    restart "jira"
#
#}

function process_status() {
  if [ $1 ]
  then
    service=$1
    PID=`ps -ef | grep "$service.*java" | grep -v grep | awk '{print $2}'` 2>&1
    if [ "$PID" != "" ]
    then
      echo $PID
      return 0
    fi
  fi
  return 1
}
  
function status() {
  service=$1
  pid=`process_status $service`
  process_result=$?
  if [ $process_result -eq 0 ]; then
    echo "Service $service is running ($pid)"
  else
    echo "Service $service is not running"
  fi
}

function status_all() {
#  status confluence
  status jira
#  status crowd
}

case $1 in
  start)
    start jira
    ;;
  stop)
    stop jira
    ;;
  restart)
    restart jira
    ;;
  status)
    status jira
    ;;
  *)
    application=$1
    validate_application $application
    result=$?
    if [ $result -eq 0 -a -n "$2" ]
    then 
      RETVAL=0
      case "$2" in 
        start)
          start $application
          ;;
        stop)
          stop $application
          ;;
        restart)
          restart $application
          ;;
        status)
          status $application
          ;;
        *)
          RETVAL=1
          ;;
        esac
     else
       RETVAL=1
     fi
     if [ "$RETVAL" -ne 0 ]
     then
       echo "usage: $0 [application] {start|stop|restart|status}" >&2
     fi
     ;;

esac
exit $RETVAL
