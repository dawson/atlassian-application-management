#!/bin/bash
if [ $1 ] 
then
  case $1 in
    jira) 
      $HOME/bin/shutdown.sh $1; $HOME/bin/startup.sh $1
    ;;
    confluence)
      $HOME/bin/shutdown.sh $1; $HOME/bin/startup.sh $1
    ;;
    crowd)
      $HOME/bin/shutdown.sh $1; $HOME/bin/startup.sh $1
    ;;
    *) echo "Usage: restart [confluence|crowd|jira]"
    ;;
  esac
fi

