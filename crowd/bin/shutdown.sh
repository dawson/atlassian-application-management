#!/bin/bash
if [ $1 ] 
then
  case $1 in
    jira) 
      $JIRA_INSTALL/bin/shutdown.sh
    ;;
    confluence)
      $CONF_HOME/bin/shutdown.sh
    ;;
    crowd)
      $CROWD_HOME/apache-tomcat/bin/shutdown.sh
    ;;
    *) echo "Usage: shutdown [confluence|crowd|jira]"
    ;;
  esac
fi

