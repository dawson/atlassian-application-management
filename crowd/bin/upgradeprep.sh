#!/bin/bash
#Crowd only
APPBASE=/apps/atlassian
CUSTOM_DIR=/apps/home/crowd/customizations
APP_HOME=crowd
CROWD_HOME=/apps/atlassian/crowd-home

retval=1
if [ $# -eq 1 ]; then
  BASE=$APPBASE/crowd/crowd-$1

  #Set ourselves up for success: create the dir if needed and untar the base stuff
  mkdir -p $BASE
  cd $BASE
  tar -xzf ~/atlassian-crowd-$1.tar.gz --strip-components=1
  retval=$?
  if [ $retval -ne 0 ]; then
    exit $retval
  fi

  #Run the oemprep script to create the OEM files and add the images we want
  /apps/home/crowd/bin/oemprep.sh $1
  retval=$?
  if  [ $retval -ne 0 ]; then
    exit $retval
  fi

  #Run the patches to apply NI&S-specific configuration
  patch $BASE/apache-tomcat/conf/server.xml $CUSTOM_DIR/server.xml.patch
  patch $BASE/$APP_HOME/WEB-INF/classes/crowd-init.properties $CUSTOM_DIR/crowd-init.properties.patch
  patch $BASE/build.properties $CUSTOM_DIR/build.properties.patch
  
  # If CROWD_HOME/crowd.cfg.xml doesn't exist, add it.  Not needed for upgrades, just for fresh installs 
  #cp $CUSTOM_DIR/crowd.cfg.xml $CROWD_HOME 
  echo "Done."
  retval=0

else
  echo "Usage: upgradeprep <version>"
  retval=1
fi
