#!/bin/bash
BASE=/apps/atlassian/crowd
APP_HOME=crowd-webapp
if [ $# -eq 1 ]; then
	BASE=$BASE/crowd-$1
    
	echo "Prepping $BASE..."
	cp $BASE/apache-tomcat/conf/server.xml $BASE/apache-tomcat/conf/server.xml.OEM
	cp $BASE/$APP_HOME/WEB-INF/classes/crowd-init.properties $BASE/$APP_HOME/WEB-INF/classes/crowd-init.properties.OEM
    cp $BASE/build.properties $BASE/build.properties.OEM

	echo "Done."
else
        echo "Usage: oemprep <version>"
fi

