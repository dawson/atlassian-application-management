#!/bin/bash

function process_status() {
  if [ $1 ]
  then
    service=$1
    PID=`ps -ef | grep "$service.*java" | grep -v grep | awk '{print $2}'` 2>&1
    if [ "$PID" != "" ]
    then
      echo $PID
      return 0
    fi
  fi
  return 1
}
  
function status() {
  service=$1
  pid=`process_status $service`
  process_result=$?
  if [ $process_result -eq 0 ]; then
    echo "Service $service is running ($pid)"
  else
    echo "Service $service is not running"
  fi
}

function status_all() {
  status confluence
  status jira
  status crowd
}

if [ $1 ]
then
  case $1 in
    confluence)
      status confluence
      ;;
    jira)
      status jira
      ;;
    docs)
      status confluence
      ;;
    tasks)
      status jira
      ;;
    crowd)
      status crowd
      ;;
    sso)
      status crowd
      ;;
    all)
      status_all
      ;;
    *)
      echo "usage: status [confluence|docs|jira|tasks|crowd|sso|all]"
      ;;
  esac
else
  status_all
fi
