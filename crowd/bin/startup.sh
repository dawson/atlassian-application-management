#!/bin/bash
if [ $1 ] 
then
  case $1 in
    jira) 
      $JIRA_INSTALL/bin/startup.sh
    ;;
    confluence)
      $CONF_HOME/bin/startup.sh
    ;;
    crowd)
      $CROWD_HOME/apache-tomcat/bin/startup.sh
    ;;
    *) echo "Usage: startup [confluence|crowd|jira]"
    ;;
  esac
fi

