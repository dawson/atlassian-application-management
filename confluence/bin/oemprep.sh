#!/bin/bash
#Confluence only
APPBASE=/apps/atlassian/confluence
APP_HOME=confluence
if [ $# -eq 1 ]; then
  BASE=$APPBASE/confluence-$1

  echo "Prepping $BASE ..."
  cp $BASE/conf/server.xml $BASE/conf/server.xml.OEM
  cp $BASE/bin/setenv.sh $BASE/bin/setenv.sh.OEM
#  cp $BASE/$APP_HOME/WEB-INF/web.xml $BASE/$APP_HOME/WEB-INF/web.xml.OEM
  cp $BASE/$APP_HOME/WEB-INF/classes/seraph-config.xml $BASE/$APP_HOME/WEB-INF/classes/seraph-config.xml.OEM
  cp $BASE/$APP_HOME/WEB-INF/classes/crowd.properties $BASE/$APP_HOME/WEB-INF/classes/crowd.properties.OEM
  cp $BASE/$APP_HOME/WEB-INF/classes/confluence-init.properties $BASE/$APP_HOME/WEB-INF/classes/confluence-init.properties.OEM
    
  #Image and icon stuff.  OEMs first, then we'll copy out of a dir because these don't change.
  cp $BASE/$APP_HOME/favicon.ico $BASE/$APP_HOME/favicon.ico.OEM
  cp $BASE/$APP_HOME/images/icons/favicon.png $BASE/$APP_HOME/images/icons/favicon.png.OEM
  cp $BASE/$APP_HOME/images/icons/favicon.ico $BASE/$APP_HOME/images/icons/favicon.ico.OEM
  
  #Add in the fixed helpful components
  cp ~/customizations/favicon.ico $BASE/$APP_HOME/favicon.ico
  cp ~/customizations/images-icons-favicon.ico $BASE/$APP_HOME/images/icons/favicon.ico
  cp ~/customizations/images-icons-favicon.png $BASE/$APP_HOME/images/icons/favicon.png
  
  echo "Done."
  exit 0

else
  echo "Usage: oemprep <version>"
  exit 1
fi
