#!/bin/bash
#Confluence only
APPBASE=/apps/atlassian
CUSTOM_DIR=/apps/home/wiki/customizations
APP_HOME=confluence

retval=1
if [ $# -eq 1 ]; then
  BASE=$APPBASE/confluence/confluence-$1

  #Set ourselves up for success: create the dir if needed and untar the base stuff
  mkdir -p $BASE
  cd $BASE
  tar -xzf ~/atlassian-confluence-$1.tar.gz --strip-components=1
  retval=$?
  if [ $retval -ne 0 ]; then
    exit $retval
  fi

  #Run the oemprep script to create the OEM files and add the images we want
  /apps/home/wiki/bin/oemprep.sh $1
  retval=$?
  if  [ $retval -ne 0 ]; then
    exit $retval
  fi

  #Run the patches to apply NI&S-specific configuration
  patch $BASE/conf/server.xml $CUSTOM_DIR/server.xml.patch
  patch $BASE/bin/setenv.sh $CUSTOM_DIR/setenv.sh.patch
  patch $BASE/$APP_HOME/WEB-INF/classes/seraph-config.xml $CUSTOM_DIR/seraph-config.xml.patch
  patch $BASE/$APP_HOME/WEB-INF/classes/crowd.properties $CUSTOM_DIR/crowd.properties.patch
  patch $BASE/$APP_HOME/WEB-INF/classes/confluence-init.properties $CUSTOM_DIR/confluence-init.properties.patch
   
  echo "Done."
  retval=0

else
  echo "Usage: upgradeprep <version>"
  retval=1
fi
